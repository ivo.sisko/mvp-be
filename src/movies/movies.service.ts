import { genres } from './../constants/genres';
import { MovieI } from './models/movie.interface';
import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from 'nestjs-http-promise';
import { AppController } from 'src/app.controller';
import { sortObjectsByProp } from 'src/utils/sortObjectsByProp';
import { filter } from 'rxjs';

@Injectable()
export class MoviesService {
  private readonly logger = new Logger(AppController.name);
  constructor(private httpService: HttpService) {}

  async getMovies(
    page: string,
    sort_by?: string,
    filter_by?: string,
    query?: string,
  ): Promise<MovieI[]> {
    let response;
    if (query) {
      response = await this.httpService.get(
        `https://api.themoviedb.org/3/search/movie?api_key=${process.env.MOVIES_API_KEY}&query=${query}`,
      );
    } else {
      response = await this.httpService.get(
        `https://api.themoviedb.org/3/movie/top_rated?api_key=${process.env.MOVIES_API_KEY}&page=${page}`,
      );
    }

    const results = response.data.results;
    let sorted = results;
    if (sort_by === 'name_asc') {
      sorted = sortObjectsByProp(results, 'title');
    } else if (sort_by === 'name_desc') {
      sorted = sortObjectsByProp(results, 'title', false);
    } else if (sort_by === 'year_asc') {
      sorted = sortObjectsByProp(results, 'release_date');
    } else if (sort_by === 'year_desc') {
      sorted = sortObjectsByProp(results, 'release_date', false);
    }
    if (filter_by) {
      const filters = JSON.parse(filter_by);

      let filtered = sorted;
      if (filters.genre !== '') {
        filtered = filtered.filter((movie) =>
          movie.genre_ids.includes(Number(filters.genre)),
        );
      }
      if (filters.year !== '') {
        filtered = filtered.filter(
          (movie) =>
            String(new Date(movie.release_date).getFullYear()) === filters.year,
        );
      }
      sorted = filtered;
    }
    return { ...response.data, results: sorted, sortBy: sort_by };
  }

  async search(query: string): Promise<MovieI[]> {
    const response = await this.httpService.get(
      `https://api.themoviedb.org/3/search/movie?api_key=${process.env.MOVIES_API_KEY}&query=${query}`,
    );
    return response.data;
  }
}
