import { PostFavouriteDto } from './dto/post-favourite.dto';
import { MovieI } from './../movies/models/movie.interface';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { FavouriteService } from './favourite.service';
import {
  Body,
  Controller,
  Delete,
  Get,
  Post,
  UseGuards,
  Param,
  Request,
} from '@nestjs/common';

@Controller('favourites')
export class FavouriteController {
  constructor(private favouriteService: FavouriteService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  async add(
    @Body() movie: PostFavouriteDto,
    @Request() req: any,
  ): Promise<any> {
    return await this.favouriteService.add({ ...movie, user: req.user.userId });
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  async get(@Request() req: any): Promise<MovieI[]> {
    const userId = req.user.userId;
    console.log('userId', userId);
    return await this.favouriteService.findAll(userId);
  }

  @Delete('delete/:movieId')
  @UseGuards(JwtAuthGuard)
  async remove(@Param() params: any): Promise<any> {
    return await this.favouriteService.remove(Number(params.movieId));
  }
}
