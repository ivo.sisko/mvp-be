import { FavouriteI } from './models/favourite.interface';
import { MovieEntity } from './../movies/models/movie.entity';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class FavouriteService {
  constructor(
    @InjectRepository(MovieEntity)
    private favouritesRepository: Repository<MovieEntity>,
  ) {}

  async add(movie: FavouriteI): Promise<FavouriteI> {
    return await this.favouritesRepository.save(movie);
  }

  async findAll(userId: number): Promise<FavouriteI[]> {
    return await this.favouritesRepository.find({ where: { user: userId } });
  }

  async remove(movieId: number): Promise<any> {
    return await this.favouritesRepository.delete({ id: movieId });
  }
}
